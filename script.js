
let array = [9, 8, 7, 6]

function soma__dois(elemento) {
    elemento += 2
    }

function maiorQueDez(elemento) {
    return elemento > 10
}

function newForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        
        callback(array[index], index, array)
    }
 }

 function newMap(array, callback) {
    let newarray = []
    for (let index = 0; index < array.length; index++) {
       newarray.push(callback(array[index], index, array))
    }
    return newarray
 }

 const newSome = (callback, array) => {
     let bolean = false
    for (let index = 0; index < array.length; index++) {
        
       bolean = callback(array[index], index, array)

       if(bolean === true){

           return bolean
       }
    }
    return bolean
 }

 const newFind = (callback, array) => {
    for (let index = 0; index < array.length; index++) {
          if(callback(array[index], index, array)=== true){
            return array[index]
        }
    }
    return undefined
 }

 const newFindIndex = (callback, array) => {
    for (let index = 0; index < array.length; index++) {
        if(callback(array[index], index, array)=== true){
          return index
      }
  }
  return -1
}

const newEvery = (callback, array) => {
    for (let index = 0; index < array.length; index++) {
        if(callback(array[index], index, array) !== true){
          return false
      }
  }
  return true
}
 
const newFilter = (callback, array) => {
    let newarray = []
    for (let index = 0; index < array.length; index++) {
        if(callback(array[index], index, array) === true){
            newarray.push(array[index])
      }
  }
  return newarray
}